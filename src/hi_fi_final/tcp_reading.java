/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hi_fi_final;

//import static hi_fi_final.Hi_Fi_Final.aus_data;
import static hi_fi_final.Hi_Fi_Final.QUIT_APP;
import static hi_fi_final.Hi_Fi_Final.aus_output;
import static hi_fi_final.Hi_Fi_Final.message;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;


public class tcp_reading implements Runnable{
    public static Socket soc;
//    private final String ServerIP = "epionex.com";
    private final String ServerIP = "localhost";
    private final int ServerSocket = 1235;
    public static int socnum = 0;
    public static boolean TCP_Thread_Run = true;
    public static byte[] da4 = new byte[128000];
    public static float mul;
    public static byte [] start = new byte[2];

    @Override
    public void run() {
        int MSB=0;
        int LSB=0;
        int temp1=0;
        byte[] temp_data = new byte[2];
        byte [] b= new byte[8000];
        AudioFormat af = new AudioFormat(32000, 16, 2, true, false);
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, af);
        SourceDataLine line = null;
        try {
            line = (SourceDataLine) AudioSystem.getLine(info);
            line.open(af,128000);
            }
        catch (LineUnavailableException ex) {
            Logger.getLogger(tcp_reading.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Audio Playback not available. Please retry");
            message = "Audio Playback not available. Please retry";
            return;
            }
        line.start();
        
        try 
        {
            aus_output.flush();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(tcp_reading.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error in Flushing the Pipe. Please retry");
            message = "Error in Flushing the Pipe. Please retry";
            return;
        }
        while(!QUIT_APP) //QUIT_APP boolean variable will be true when EXIT button is pressed. 
        {
            System.out.println("Trying to connect the server....");
            message = "Trying to connect the server....";
            try 
            {
                TCP_Thread_Run = true;
                if(startconnection()==true)
                {
                    System.out.println("Server Connected..");
                    message = "Server Connected..";
                }else
                {
                    System.out.println("Server Connection Failed");
                    message = "Server Connection Failed";
                    TCP_Thread_Run = false;
                }
                    
                while (TCP_Thread_Run) 
                {
                    int read1 = soc.getInputStream().read(b);
                    if(read1 == -1) break; 
                        /*This condition is true when the remote side is exiting
                            and the server is exited. So that a vaild socket is 
                            not available.
                        */

                    if(read1>0)
                    {
                        for (int k = 0; k < read1; )
                        {
                            LSB = b[k++];
                            MSB = b[k++];//one byte
                                //b+=(8000.0/samples_taken);
                                //b+=40;
                            temp1 = ((MSB << 8) | (LSB & 0x0FF));
                            if ((temp1 & 0x8000) == 0x8000) 
                            {
                                //if sign bit is one ie value is negative.
                                //taking 2's compliment of the value(excluding sign bit)
                                temp1 = ~temp1;
                                temp1 &= 0x7FFF;
                                temp1++;
                                temp1 = -1 * temp1;
                            }
                            temp_data[0] = (byte)(temp1 & 0x0FF);
                            temp_data[1] = (byte)(temp1>>8);
                            aus_output.write(temp_data,0,2);
                        }
                        int size = 8000;
                
                        byte[] a1 = new byte[size*2];
                        a1 = upSampling(b);
                        byte[] a2 = new byte[size*2*2];
                        a2 = upSampling(a1);
                        byte[] a3 = new byte[size*2*2*2];
                        a3 = upSampling(a2);
                        byte[] a4 = new byte[size*2*2*2*2];
                        a4 = upSampling(a3);
                            //byte[] da4 = new byte[size*2*2*2*2];
                        da4 = amplification(a4,2*mul);
                        line.write(da4, 0, da4.length);
                    }
                }
            } catch (Exception e) 
            {
                e.printStackTrace();
                System.out.println("Exception "+e);
            }
            System.out.println("Consultation Stopped");
            message = "Consultation Stopped";
            try 
            {
                soc.getOutputStream().write(256);
                Thread.sleep(1000);
            } catch (Exception ex) 
            {
                Logger.getLogger(tcp_reading.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //return;
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private static byte[] upSampling(byte[] b1)
    {
        int p=0,k=0;
        int size = b1.length;
        int sample_val=0;
        byte[] n = new byte[size*2];
        for(int j=0;j<b1.length;)
        {
            if(j+2==b1.length)
                sample_val = AvFn(b1[j],b1[j+1],b1[j],b1[j+1]);
            else
                sample_val = AvFn(b1[j],b1[j+1],b1[j+2],b1[j+3]);
            n[k++]=b1[j];
            n[k++]=b1[j+1];
            n[k++]=(byte)(sample_val);
            n[k++]=(byte)(sample_val>>8);
            j+=2;
        }
        return n;
    }
    
    private static byte[] amplification(byte[] d1,float m)
    {
        int size = d1.length;
        int temp1=0,LSB1=0,MSB1=0;
        int amp=0;
        byte[] g = new byte[size];
        for(int i=0;i<size;i++)
        {
            LSB1 = d1[i];
            MSB1 = d1[i+1];
            temp1 = ((MSB1<<8)|(LSB1 & 0xFF));
            if((temp1 & 0x8000) == 0x8000)
            {
                temp1 = ~temp1;
                temp1 &= 0x7FFF;
                temp1++;
                temp1 =-1*temp1;
            }
            amp=(int)(temp1*m);
            g[i]=(byte)(amp);
            g[i+1]=(byte)(amp>>8);
            i++;
        }
        return g;
    }
    
    private static int AvFn(byte c1,byte c2,byte c3,byte c4)
    {
        int temp1=0,temp2=0,LSB1=0,MSB1=0,LSB2=0,MSB2=0,avg=0;
        LSB1 = c1;
        MSB1 = c2;
        LSB2 = c3;
        MSB2 = c4;
        temp1 = ((MSB1<<8)|(LSB1 & 0xFF));
        if((temp1 & 0x8000) == 0x8000)
        {
            temp1 = ~temp1;
            temp1 &= 0x7FFF;
            temp1++;
            temp1 =-1*temp1;
        }
           
        temp2 = ((MSB2<<8)|(LSB2 & 0xFF));
        if((temp2 & 0x8000) == 0x8000)
        {
            temp2 = ~temp2;
            temp2 &= 0x7FFF;
            temp2++;
            temp2 =-1*temp2;
        }
        avg = (temp1 + temp2)/2;
        return avg;
    }

    private boolean startconnection() {
        try 
        {
            soc = new Socket(ServerIP, ServerSocket);
            Thread connector = new Thread()
            {
                public void run()
                {
                    try {
                        soc.getOutputStream().write(start);
                    } catch (IOException ex) {
                        Logger.getLogger(tcp_reading.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    reader.start(); 
                }
            };
            connector.start();
            
            return true;
            
        } catch (IOException ex) 
        {
            return false;
        }
    }
    
Thread reader = new Thread()
    {
        byte buffer[] = new byte[1000];
        int length;
        public void run()
        {
            try 
            {
                while((length = soc.getInputStream().read(buffer))>0)
                {
                    if(length == 2 && buffer[0]== 'E')
                    {
                        char ch = (char) buffer[1];
                        System.out.println("test:" +ch);
                        int i = buffer[1];
                        i++;
                        start[1] = buffer[1];
//                        jLabel2.setText("T no: "+i);
                    }
                    else
                    {
                        System.out.println("data receiving");
//                        jLabel3.setText("data...");
                        sleep(1000);
//                        jLabel3.setText("");
                        
                    }
                }
//                jButton1.setEnabled(true);
                soc.close();
            } catch (IOException ex) 
            {
                try 
                {
                    soc.close();
                } catch (IOException ex1) 
                {
//                    Logger.getLogger(Doctor.class.getName()).log(Level.SEVERE, null, ex1);
                }
//                jButton1.setEnabled(true);
//                Logger.getLogger(Doctor.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
//                Logger.getLogger(Doctor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    };
    
}
